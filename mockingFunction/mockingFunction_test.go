package mockingfunction

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

type Test struct {
	title    string
	mockFunc func()
	testFunc func(tt *testing.T)
}

func TestDialog(t *testing.T) {
	tests := []Test{
		{
			title:    "Should doing default function",
			mockFunc: func() {},
			testFunc: func(tt *testing.T) {
				assert := assert.New(tt)
				result1, result2 := dialog()
				assert.Equal(result1, "hello roy", "the world must be same")
				assert.Equal(result2, "hi agni", "the world must be same")
			},
		},
		{
			title: "Should doing mocked function",
			mockFunc: func() {
				hello = func(name string) string {
					return "Hello " + name + ", Wellcome to the world."
				}
			},
			testFunc: func(tt *testing.T) {
				assert := assert.New(tt)
				result1, result2 := dialog()
				assert.Equal(result1, "Hello roy, Wellcome to the world.", "the world must be same")
				assert.Equal(result2, "hi agni", "the world must be same")
			},
		},
	}
	// backup MockFunc
	oriHello := hello
	// test process
	testProcess(t, tests)
	// restore MockFunc
	hello = oriHello
}

// local
func testProcess(t *testing.T, tests []Test) {
	for _, test := range tests {
		t.Run(test.title, func(tt *testing.T) {
			test.mockFunc()
			test.testFunc(tt)
		})
	}
}
