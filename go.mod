module gitlab.com/roy.nasrul17/poc-kastela-data-migration

go 1.16

require (
	github.com/alitto/pond v1.5.1
	github.com/goombaio/namegenerator v0.0.0-20181006234301-989e774b106e
	github.com/satori/go.uuid v1.2.0
	github.com/stretchr/testify v1.7.0
	gorm.io/driver/postgres v1.1.1
	gorm.io/gorm v1.21.15
)
